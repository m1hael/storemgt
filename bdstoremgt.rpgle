**FREE

///
// BlueDroplet Demo : Store Management
//
// This demo shows how to use the BlueDroplet REST framework.
//
// \author Mihael Schmidt
// \date 2016-12-27
///


ctl-opt main(main);


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
dcl-pr main extpgm('BDSTOREMGT') end-pr;

/include 'bluedroplet/bluedroplet_h.rpgle'


//-------------------------------------------------------------------------------------------------
// Program Entry Point
//-------------------------------------------------------------------------------------------------
dcl-proc main;
  dcl-s service pointer;
  dcl-ds configuration likeds(droplet_config_configuration);
  
  service = droplet_service_create();

  configuration = droplet_config_xml_load('/home/mschmidt/src/storemgt/droplet.xml');
  droplet_service_setConfiguration(service : configuration);

  droplet_service_addEndPoint(service : %paddr('storemgt_rest_getStore') : '/storemgt/store' : DROPLET_GET);
  droplet_service_addEndPoint(service : %paddr('storemgt_rest_listStores') : '/storemgt/stores' : DROPLET_GET);
  droplet_service_addEndPoint(service : %paddr('storemgt_rest_updateStore') : '/storemgt/store' : DROPLET_POST);
  droplet_service_addEndPoint(service : %paddr('storemgt_rest_createStore') : '/storemgt/store' : DROPLET_PUT);
  droplet_service_addEndPoint(service : %paddr('storemgt_rest_deleteStore') : '/storemgt/store' : DROPLET_DELETE);

  droplet_service_start(service);

  droplet_service_finalize(service);
end-proc;
