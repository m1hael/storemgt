# BlueDroplet Example : Store Management #

This project shows how to create REST services with the framework BlueDroplet.

Another source to getting started with creating REST services with BlueDroplet
is the [Getting Started](https://bitbucket.org/m1hael/bluedroplet/wiki/Getting_Started) 
page in the project wiki of [BlueDroplet](https://bitbucket.org/m1hael/bluedroplet).

## Building ##
The project stores the source code in the IFS in stream files. The *make*
tool is used to build the project. *BIN_LIB* and *INCLUDE* are variables in the
Makefile which can be overwritten with your own values.

BIN_LIB: the library for modules and binding directory

INCLUDE: the IFS folder for the header files / copy books

```
make BIN_LIB=MSCHMIDT INCLUDE=/usr/local/include
```

## Who do I talk to? ##

Mihael Schmidt , mihael@rpgnextgen.com
