DELETE FROM stores;

INSERT INTO stores(name, address, longitude, latitude) VALUES
('Frecken Furniture Frechen', 'Europaallee 1, D-50226 Frechen' , 50.9179151, 6.8332905 ),
('Frecken Furniture Porta', 'Feldstrasse 20, D-32457 Porta Westfalica-Barkhausen' , 52.2595012, 8.8991348 ),
('Frecken Furniture Bielefeld', 'Bielitzer Strasse 40, D-33699 Bielefeld' , 51.998701, 8.6074463 ),
('Frecken Furniture Hannover', 'Opelstrasse 9, D-30916 Hannover' , 52.4257148, 9.8280623 ),
('Frecken Furniture Magdeburg', 'Salbker Chaussee 65, D-39118 Magdeburg' , 52.08289,11.5849013 ),
('Frecken Furniture Leipzig', 'Alte Messe 2, D-04103 Leipzig' , 51.3181072, 12.4007162 ),
('Frecken Furniture Aachen', 'Krefelder Strasse/Am Gut Wolf 2, D-52070 Aachen' , 50.7903229, 6.0965662 ),
('Frecken Furniture Bornheim', 'Alexander-Bell-Strasse 2, D-53332 Bornheim' , 50.75819, 7.0240213 ),
('Frecken Furniture Braunschweig', 'Hansestrasse 28, D-38112 Braunschweig' , 52.30986, 10.4991913 ),
('Frecken Furniture Dessau', 'Ernst-Zindel-Strasse 5, D-06847 Dessau-Rosslau' , 51.81779, 12.1722913 ),
('Frecken Furniture Gorlitz', 'Robert-Bosch-Strasse 1, D-02828 Gorlitz' , 51.1885, 14.9559813 ),
('Frecken Furniture Gutersloh', 'Friedrich-Ebert-Strasse 101, D-33332 Gutersloh' , 51.9068278, 8.3878806 ),
('Frecken Furniture Halberstadt', 'Im Sulzeteiche 3, D-38820 Halberstadt' , 51.88206, 11.0739313 ),
('Frecken Furniture Jena', 'Weimarische Strasse 3b, D-07751 Jena-Isserstedt' , 50.9577575, 11.506836 ),
('Frecken Furniture Porz-Lind', 'Portastrasse, D-51147 Porz-Lind' , 50.8479907, 7.0921693 ),
('Frecken Furniture Neuwied', 'Breslauer Strasse 88, D-56566 Neuwied' , 50.4396183, 7.4898682 ),
('Frecken Furniture Potsdam', 'Zum Kirchsteigfeld 4, D-14480 Potsdam' , 52.3690345, 13.1264603 ),
('Frecken Furniture Stendal', 'Heerener Strasse 79, D-39576 Stendal' , 52.5837892, 11.8714501 ),
('Frecken Furniture Wallenhorst', 'Borsigstrasse 1, D-49134 Wallenhorst' , 52.35569, 8.0044613 ),
('Frecken Furniture Wiedemar', ' Lilienthalstrasse 7-9, D-04509 Wiedemar' , 51.46697, 12.2119313 ),
('Frecken Furniture Zwickau', 'Schneeberger Strasse 100, D-08056 Zwickau' , 50.7070781, 12.4946337 );
