**FREE

///
// BlueDroplet Demo : Store REST Endpoints
//
// \author Mihael Schmidt
// \date 2016-12-28
///


ctl-opt nomain;


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
/include 'bdstore_h.rpgle'
/include 'bluedroplet/bluedroplet_h.rpgle'
/include 'json/json_h.rpgle'
/include 'llist_h.rpgle'
/include 'libc_h.rpgle'

dcl-pr storemgt_rest_getStore extproc('storemgt_rest_getStore');
  service pointer const;
  connection pointer const;
  message pointer const;
end-pr;

dcl-pr storemgt_rest_updateStore extproc('storemgt_rest_updateStore');
  service pointer const;
  connection pointer const;
  message pointer const;
end-pr;

dcl-pr storemgt_rest_deleteStore extproc('storemgt_rest_deleteStore');
  service pointer const;
  connection pointer const;
  message pointer const;
end-pr;

dcl-pr storemgt_rest_createStore extproc('storemgt_rest_createStore');
  service pointer const;
  connection pointer const;
  message pointer const;
end-pr;

dcl-pr storemgt_rest_listStores extproc('storemgt_rest_listStores');
  service pointer const;
  connection pointer const;
  message pointer const;
end-pr;

dcl-pr storeToJson varchar(1024);
  store likeds(storemgt_store_t) const;
end-pr;


//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------
dcl-proc storemgt_rest_getStore export;
  dcl-pi *N;
    service pointer const;
    connection pointer const;
    message pointer const;
  end-pi;
  
  dcl-s json pointer;
  dcl-s s pointer;
  dcl-s storeId int(10);
  dcl-s charId char(10);
  dcl-ds store likeds(storemgt_store_t);
  dcl-s data varchar(512);
  dcl-s path char(1024);
  dcl-s pathLength int(10);
  
  path = droplet_http_getUri(message);
  pathLength = %len(%trimr(path));
  
  // get store id from path
  charId = droplet_service_getLastPathSegment(%addr(path) : pathLength);
  if (charId = *blank);
    droplet_service_send(connection : DROPLET_BAD_REQUEST : 'Could not retrieve store id from URI.' : DROPLET_TEXT);
    return;
  else;
    storeId = %int(charId);
  endif;

  // get store from internal service program
  store = storemgt_getStore(storeId);
  
  if (store.id = 0);
    droplet_service_send(connection : DROPLET_NOT_FOUND);
  else;
    // create json from store data structure
    data = storeToJson(store);
  
    // send json to client (buffered)
    droplet_service_ok(connection : data : DROPLET_JSON);
  endif;
end-proc;

dcl-proc storemgt_rest_updateStore export;
  dcl-pi *N;
    service pointer const;
    connection pointer const;
    message pointer const;
  end-pi;
  
  dcl-s json pointer;
  dcl-s bodyPtr pointer;
  dcl-s ptr pointer;
  dcl-ds store likeds(storemgt_store_t);
  dcl-ds messageBody likeds(droplet_buffer);
  
  messageBody = droplet_http_getBody(message);
  
  bodyPtr = %alloc(messageBody.length + 1);
  memset(bodyPtr : x'00' : messageBody.length + 1);
  memcpy(bodyPtr : messageBody.string : messageBody.length);
  
  json = json_parse(bodyPtr);
  
  dealloc bodyPtr;
  
  clear store;
  if (json_contains(json : 'id'));
    store.id = json_getInt(json : 'id');
  endif;
  if (json_contains(json : 'name'));
    store.name = %str(json_getString(json : 'name'));
  endif;
  if (json_contains(json : 'address'));
    store.address = %str(json_getString(json : 'address'));
  endif;
  if (json_contains(json : 'longitude'));
    store.longitude = json_getDouble(json : 'longitude');
  endif;
  if (json_contains(json : 'latitude'));
    store.latitude = json_getDouble(json : 'latitude');
  endif;
  
  storemgt_updateStore(store);
  
  droplet_service_ok(connection);
end-proc;

dcl-proc storemgt_rest_deleteStore export;
  dcl-pi *N;
    service pointer const;
    connection pointer const;
    message pointer const;
  end-pi;
  
  dcl-s storeId int(10);
  dcl-s charId char(10);
  dcl-ds store likeds(storemgt_store_t);
  dcl-s data varchar(512);
  dcl-s path char(1024);
  dcl-s pathLength int(10);
  
  path = droplet_http_getUri(message);
  pathLength = %len(%trimr(path));
  
  // get store id from path
  charId = droplet_service_getLastPathSegment(%addr(path) : pathLength);
  if (charId = *blank);
    droplet_service_send(connection : DROPLET_BAD_REQUEST : 'Could not retrieve store id from URI.' : DROPLET_TEXT);
    return;
  else;
    storeId = %int(charId);
  endif;
  
  storemgt_deleteStore(storeId);
  
  droplet_service_ok(connection);
end-proc;

dcl-proc storemgt_rest_createStore export;
  dcl-pi *N;
    service pointer const;
    connection pointer const;
    message pointer const;
  end-pi;
  
  dcl-s json pointer;
  dcl-s bodyPtr pointer;
  dcl-s ptr pointer;
  dcl-ds store likeds(storemgt_store_t);
  dcl-ds messageBody likeds(droplet_buffer);
  
  messageBody = droplet_http_getBody(message);
  
  bodyPtr = %alloc(messageBody.length + 1);
  memset(bodyPtr : x'00' : messageBody.length + 1);
  memcpy(bodyPtr : messageBody.string : messageBody.length);
  
  json = json_parse(bodyPtr);
  
  dealloc bodyPtr;
  
  clear store;
  if (json_contains(json : 'name'));
    store.name = %str(json_getString(json : 'name'));
  endif;
  if (json_contains(json : 'address'));
    store.address = %str(json_getString(json : 'address'));
  endif;
  if (json_contains(json : 'longitude'));
    store.longitude = json_getDouble(json : 'longitude');
  endif;
  if (json_contains(json : 'latitude'));
    store.latitude = json_getDouble(json : 'latitude');
  endif;
  
  storemgt_createStore(store);
  
  droplet_service_ok(connection);
end-proc;

dcl-proc storemgt_rest_listStores export;
  dcl-pi *N;
    service pointer const;
    connection pointer const;
    message pointer const;
  end-pi;
  
  dcl-c LEFT_BRACKET x'BA';
  dcl-c RIGHT_BRACKET x'BB';
  dcl-s data varchar(1024);
  dcl-s headers pointer;
  dcl-s stores pointer;
  dcl-s ptr pointer;
  dcl-ds store likeds(storemgt_store_t) based(ptr);
  
  stores = storemgt_listStores();
  
  headers = list_create();
  list_addString(headers : 'Transfer-Encoding: chunked');

  droplet_service_sendHead(connection : DROPLET_OK : DROPLET_JSON : headers);

  data = '{ ''stores'' : ' + LEFT_BRACKET;
  droplet_service_sendChunk(connection : %addr(data : *DATA) : %len(data));
  
  // send stores
  ptr = list_iterate(stores);
  dow (ptr <> *null);
    data = storeToJson(store);
    droplet_service_sendChunk(connection : %addr(data : *DATA) : %len(data));
     
    ptr = list_iterate(stores);
  enddo;
  
  data = ' ' + RIGHT_BRACKET + ' }';
  droplet_service_sendChunk(connection : %addr(data : *DATA) : %len(data));

  // signal the end of the data set by sending an empty chunk.
  data = '';
  droplet_service_sendChunk(connection : %addr(data : *DATA) : %len(data));

  droplet_service_setFlag(connection : MONGOOSE_FLAG_SEND_AND_CLOSE);
end-proc;

dcl-proc storeToJson;
  dcl-pi *N varchar(1024);
    store likeds(storemgt_store_t) const;
  end-pi;
  
  dcl-s data varchar(1024);
  dcl-s json pointer;
  
  json = json_create();
  json_putInt(json : 'id' : store.id);
  json_putString(json : 'name' : %trimr(store.name));
  json_putString(json : 'address' : %trimr(store.address));
  json_putDouble(json : 'longitude' : store.longitude);
  json_putDouble(json : 'latitude' : store.latitude);
  data = %str(json_toString(json : JSON_OUTPUT_PRETTY_PRINT));
  json_dispose(json);
  
  return data;
end-proc;
