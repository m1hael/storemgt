CREATE TABLE stores (                                      
  id int not null primary key generated always as identity,
  name varchar(50) not null,                               
  address varchar(255) not null,                           
  longitude double,                                        
  latitude double                                          
) rcdfmt storesr1;

CREATE TABLE items (
  id int not null primary key,
  name varchar(50) not null,
  ean char(13) not null
);

CREATE TABLE promotion (
  id int not null primary key,
  name varchar(50) not null,
  tag varchar(20) not null,
  description varchar(255),
  start date not null,
  end date not null,
);

CREATE TABLE promotion_items (
  promotion int not null,
  item int not null,
  quantity int not null,
  primary key (promotion, item)
);

CREATE TABLE promotion_item_distribution (
  promotion int not null,
  item int not null,
  store int not null,
  quantity int not null default 0,
  primary key (promotion, item, store)
);

CREATE TABLE distribution_keys (
  store int not null primary key,
  value int not null default 0
);
