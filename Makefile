#
# Build script for Store Management Demo
#

# BIN_LIB is the destination library for the program.
BIN_LIB=MSCHMIDT2
# to this library the prototype source file (copy book) is copied in the install step
INCLUDE=/home/mschmidt/include

# CFLAGS = RPG compile parameter
CFLAGS=OPTION(*SRCSTMT) DBGVIEW(*ALL) INCDIR('$(INCLUDE)') OPTIMIZE(*BASIC) STGMDL(*INHERIT)

# LFLAGS = binding parameter
# (make sure modules and service programs are in library list)
LFLAGS=BNDDIR(DROPLET) STGMDL(*INHERIT)

MODULES = BDSTOREMGT BDSTORE BDSTORERS

FROM_CCSID=37
INCLUDE_CCSID=37


.SUFFIXES: .rpgle .c .cpp

# suffix rules
.rpgle:
	system "CRTRPGMOD $(BIN_LIB)/$@ SRCSTMF('$<') $(CFLAGS)"
	
all: clean compile

BDSTOREMGT:
BDSTORE:
BDSTORERS:

compile: $(MODULES)


clean:
	-system "DLTMOD $(BIN_LIB)/BDSTOREMGT"
	-system "DLTMOD $(BIN_LIB)/BDSTORE"
	-system "DLTMOD $(BIN_LIB)/BDSTORERS"

.PHONY:
