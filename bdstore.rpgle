**FREE

///
// BlueDroplet Demo : Store Module
//
// This module provides procedures for accessing store master data.
//
// \author Mihael Schmidt
// \date 2016-12-27
///


ctl-opt nomain;


//-------------------------------------------------------------------------------------------------
// Files
//-------------------------------------------------------------------------------------------------
dcl-f STORES disk(*ext) usage(*input : *output : *update : *delete) qualified keyed;


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
/include 'llist_h.rpgle'
/include 'bdstore_h.rpgle'


//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------
dcl-proc storemgt_createStore export;
  dcl-pi *N likeds(storemgt_store_t);
    store likeds(storemgt_store_t);
  end-pi;

  dcl-ds store_out ext extname('STORES' : *OUTPUT) qualified end-ds;
  
  store_out.id = 0;
  store_out.name = store.name;
  store_out.address = store.address;
  store_out.longitude = store.longitude;
  store_out.latitude = store.latitude;
  
  write STORES.STORESR1 store_out; 

  // TOOD read created record to get id

  return store;
end-proc;

dcl-proc storemgt_getStore export;
  dcl-pi *N likeds(storemgt_store_t);
    id int(10) const;
  end-pi;

  dcl-ds store_in ext extname('STORES' : *INPUT) qualified end-ds;
  dcl-ds store likeds(storemgt_store_t);

  clear store;

  chain(n) (id) STORES store_in;
  if (%found());
    store.id = store_in.id;
    store.name = store_in.name;
    store.address = store_in.address;
    store.longitude = store_in.longitude;
    store.latitude = store_in.latitude;
  endif;

  return store;
end-proc;

dcl-proc storemgt_listStores export;
  dcl-pi *N pointer end-pi;

  dcl-ds store_in ext extname('STORES' : *INPUT) qualified end-ds;
  dcl-ds store likeds(storemgt_store_t);
  dcl-s list pointer;
  list = list_create();
  
  setll (1) STORES;
  read STORES store_in;
  dow (not %eof());
    store.id = store_in.id;
    store.name = store_in.name;
    store.address = store_in.address;
    store.longitude = store_in.longitude;
    store.latitude = store_in.latitude;
  
    list_add(list : %addr(store) : %size(store));
    
    read STORES store_in;
  enddo;

  return list;
end-proc;

dcl-proc storemgt_updateStore export;
  dcl-pi *N likeds(storemgt_store_t);
    store likeds(storemgt_store_t);
  end-pi;

  dcl-ds store_out ext extname('STORES' : *ALL) qualified end-ds;
  
  chain (store.id) STORES store_out;
  if (%found());
    store_out.name = store.name;
    store_out.address = store.address;
    store_out.longitude = store.longitude;
    store_out.latitude = store.latitude;
  
    update STORES.STORESR1 %fields(store_out.name : store_out.address : 
                                   store_out.longitude : store_out.latitude);
  else;
    // TODO send escape message
  endif;

  return store;
end-proc;

dcl-proc storemgt_deleteStore export;
  dcl-pi *N;
    id int(10) const;
  end-pi;

  dcl-ds store_in ext extname('STORES' : *INPUT) qualified end-ds;
  
  chain (id) STORES store_in;
  if (%found());
    delete STORES.STORESR1;
  endif;
end-proc;
