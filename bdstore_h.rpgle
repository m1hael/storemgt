**FREE

/if not defined(BLUEDROPLET_STORE_H)
/define BLUEDROPLET_STORE_H

///
// BlueDroplet Demo : Store Module Prototypes
//
// \author Mihael Schmidt
// \date 2016-12-28
///


//-------------------------------------------------------------------------------------------------
// Data Structures
//-------------------------------------------------------------------------------------------------
dcl-ds storemgt_store_t qualified template;
  id int(10);
  name varchar(50);
  address varchar(255);
  longitude float(8);
  latitude float(8);
end-ds;


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
dcl-pr storemgt_createStore likeds(storemgt_store_t) extproc('storemgt_createStore');
  store likeds(storemgt_store_t);
end-pr;

dcl-pr storemgt_getStore likeds(storemgt_store_t) extproc('storemgt_getStore');
  id int(10) const;
end-pr;

dcl-pr storemgt_listStores pointer extproc('storemgt_listStores') end-pr;

dcl-pr storemgt_updateStore likeds(storemgt_store_t) extproc('storemgt_updateStore');
  store likeds(storemgt_store_t);
end-pr;

dcl-pr storemgt_deleteStore extproc('storemgt_deleteStore');
  id int(10) const;
end-pr;

/endif
